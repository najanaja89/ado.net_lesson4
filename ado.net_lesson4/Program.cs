﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace ado.net_lesson4
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. База данных
            DataSet studentsDB = new DataSet("StudentDB");
            //2. Таблицы БД
            DataTable students = new DataTable("Students");
            DataTable gender = new DataTable("Gender");
            //3. Кололнки 
            InitStudentTable(ref students);
            InitGenderTable(ref gender);
             

            ForeignKeyConstraint FK_gender = new ForeignKeyConstraint(gender.Columns["Id"], students.Columns["gender"]);

            students.Constraints.Add(FK_gender);
            studentsDB.Tables.AddRange(new DataTable[]
                {
                    gender,
                    students
                });

            DataRow newRow = gender.NewRow();
            newRow["Gender"] = "male";
            gender.Rows.Add(newRow);

            newRow = gender.NewRow();
            newRow["Gender"] = "female";
            gender.Rows.Add(newRow);

            gender.WriteXml("gender.xml");

            Console.WriteLine("Enter fio");
            string fio = Console.ReadLine();
            Console.WriteLine("Enter gender");
            int intGender = int.Parse(Console.ReadLine());
            Console.ReadLine();

            FillStudents(newRow, fio, intGender, students);
        }

        private static void FillStudents(DataRow newRow, string fio, int gender, DataTable students)
        {
            newRow = students.NewRow();
            newRow["fio"] = fio;
            newRow["gender"] = gender;
            students.Rows.Add(newRow);

            students.WriteXml("gender.xml");
        }

        private static void InitGenderTable(ref DataTable gender)
        {
            DataColumn id = new DataColumn("Id", typeof(int));
            id.AllowDBNull = false;
            id.AutoIncrement = true;
            id.AutoIncrementSeed = 1;
            id.AutoIncrementStep = 1;
            id.Caption = "Indentity";
            id.Unique = true;
            id.ReadOnly = true;

            DataColumn Gender = new DataColumn("Gender", typeof(string))
            {
                AllowDBNull = false,
                Caption = "Gender",
                MaxLength = 10
                
            };

            gender.Columns.AddRange(new DataColumn[]
           {
                id,
                Gender
           });
            gender.PrimaryKey = new DataColumn[] { gender.Columns["Id"] };

        }

        private static void InitStudentTable(ref DataTable students)
        {
            DataColumn id = new DataColumn("Id", typeof(int));
            id.AllowDBNull = false;
            id.AutoIncrement = true;
            id.AutoIncrementSeed = 1;
            id.AutoIncrementStep = 1;
            id.Caption = "Indentity";
            id.Unique = true;
            id.ReadOnly = true;

            DataColumn fio = new DataColumn("FIO", typeof(string));
            fio.AllowDBNull = true;
            fio.Caption = "FIO";
            fio.MaxLength = 60;

            DataColumn genderId = new DataColumn("gender", typeof(int))
            {
                Caption = "gender",
                AllowDBNull = false
            };

            students.Columns.AddRange(new DataColumn[]
            {
                id,
                fio,
                genderId
            });
            students.PrimaryKey = new DataColumn[]{  students.Columns["Id"]};
          
           
        }
        
    }
}
